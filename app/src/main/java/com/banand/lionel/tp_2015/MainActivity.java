package com.banand.lionel.tp_2015;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.banand.lionel.tp_2015.Adapters.MenuAdapter;
import com.banand.lionel.tp_2015.dataaccess.Accessor.MediasDataAccess;
import com.banand.lionel.tp_2015.dataaccess.obj.MediaObject;
import com.banand.lionel.tp_2015.Fragments.MediasFragment;
import com.banand.lionel.tp_2015.Items.MenuMediasItem;
import com.banand.lionel.tp_2015.Utilities.Global;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MediasFragment.OnElementSelectedListener {
    //menu array
    private ArrayList<MenuMediasItem> mMenuMediasItem;
    private DrawerLayout mDrawerLayout;
    private ListView mLVMenu;
    private int mCurrentMenuSelected = 0;

    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTitle = mDrawerTitle = getTitle();
        sendBroadcast(new Intent(Global.NEED_TO_START_SERVICE));
//        startService(new Intent(this, TpAndroidService.class));

        //init properties of left menu
        mMenuMediasItem = new ArrayList<MenuMediasItem>();
        mMenuMediasItem.add(new MenuMediasItem(R.drawable.ic_action_picture, getResources().getString(R.string.images)));
        mMenuMediasItem.add(new MenuMediasItem(R.drawable.ic_action_email, getResources().getString(R.string.textes)));
        mMenuMediasItem.add(new MenuMediasItem(R.drawable.ic_action_video, getResources().getString(R.string.videos)));

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //to allow clic on icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mDrawerTitle);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerTitle = getResources().getText(R.string.drawer_open);

        mLVMenu = (ListView) findViewById(R.id.lvMediaType);

        //the adapter for the menu allow to create dynamique views for each item of the menu
        MenuAdapter menuAdapter = new MenuAdapter(getApplicationContext(), mMenuMediasItem);
        mLVMenu.setAdapter(menuAdapter);

        //when we click on an item, load the appropriate view regarding the menu selection
        mLVMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Fragment fragment = null;

                switch (i) {
                    case 0:
                        //images
                        fragment =  MediasFragment.newInstance(0, mMenuMediasItem.get(0).mIcon, mMenuMediasItem.get(0).mName);
                        break;
                    case 1:
                        //email
                        fragment = new MediasFragment();
                        ((MediasFragment) fragment).setHelloParameters(mMenuMediasItem.get(1).mName,
                                mMenuMediasItem.get(1).mIcon);
                        break;
                    case 2:
                        //video
                        fragment = new MediasFragment();
                        ((MediasFragment) fragment).setHelloParameters(mMenuMediasItem.get(2).mName,
                                mMenuMediasItem.get(2).mIcon);
                        break;
                    default:
                        break;

                }

                mCurrentMenuSelected = i;

                //display the appropriate view with a transition
                if (fragment != null) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                   /* int slide = R.animator.slide_in_left;
                    ft.setCustomAnimations(slide, R.animator.slide_out_left);*/
                    ft.replace(R.id.main_content, fragment).commit();
                    //set the selected item to the drawer
                    mLVMenu.setItemChecked(i, true);
                    mLVMenu.setSelection(i);
                    mTitle = mMenuMediasItem.get(i).mName;
                    getSupportActionBar().setTitle(mTitle);
                    mDrawerLayout.closeDrawers();
                }
            }
        });

        getMedias();

    }

    private void getMedias() {
        MediasDataAccess mediasDataAccess = new MediasDataAccess(getApplicationContext());
        for (MediaObject mediaObject : mediasDataAccess.getMediasForType(Global.MEDIA_TYPE_IMAGE)) {
            Log.e("Media name : ", mediaObject.name);
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            getSupportActionBar().setTitle(getResources().getString(R.string.drawer_close));
            return true;
        }

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onElementSelected() {
        getSupportActionBar().setTitle("Button Pressed");
    }

    @Override
    public int getSelectedMenu() {
        return mCurrentMenuSelected;
    }
}
