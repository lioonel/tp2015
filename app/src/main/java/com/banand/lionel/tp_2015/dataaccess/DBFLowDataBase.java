package com.banand.lionel.tp_2015.dataaccess;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by lionelbanand on 29/01/2017.
 */
@Database(name = DBFLowDataBase.NAME, version = DBFLowDataBase.VERSION)

public class DBFLowDataBase {
    public static final String NAME = "MyDataBase";

    public static final int VERSION = 1;
}
