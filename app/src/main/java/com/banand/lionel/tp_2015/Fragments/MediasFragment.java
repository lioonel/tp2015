package com.banand.lionel.tp_2015.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.banand.lionel.tp_2015.Activity.DisplayMediaActivity;
import com.banand.lionel.tp_2015.dataaccess.Accessor.MediasDataAccess;
import com.banand.lionel.tp_2015.dataaccess.obj.MediaObject;
import com.banand.lionel.tp_2015.Internet.TpAndroidInternet;
import com.banand.lionel.tp_2015.Manager.MediasManager;
import com.banand.lionel.tp_2015.R;

import java.util.ArrayList;


/**
 * Created by lionel on 30/09/14.
 */
//fragment representing the main right view
public class MediasFragment extends Fragment implements TpAndroidInternet.TpAndroidInternetInterface{
    OnElementSelectedListener mListener;
    MediasAdapter mAdapter;
    ListView mLVImages;
    ArrayList<MediaObject> mMediaObjects;
    Context mContext;
    final public static String MEDIA_RES_ID  = "resId";
    final public static String MEDIA_TITLE  = "title";
    final public static String MEDIA_MENU_ID  = "menuId";


    public static MediasFragment newInstance(int menuId, int resId, String text) {

        MediasFragment m = new MediasFragment();
        final Bundle args = new Bundle();
        args.putInt(MEDIA_RES_ID, resId);
        args.putString(MEDIA_TITLE, text);
        args.putInt(MEDIA_MENU_ID, menuId);

        m.setArguments(args);

        return m;
    }

    public MediasFragment(){

    }

    @Override
    public void onMediaDownloaded(MediaObject mediaObject) {

        mAdapter.notifyDataSetChanged();
        MediasDataAccess mediasDataAccess = new MediasDataAccess(getActivity());
        mediasDataAccess.insertMedia(mediaObject);
    }

    // Container Activity must implement this interface
    public interface OnElementSelectedListener {
        public void onElementSelected();
        public int getSelectedMenu();
    }

    TextView mTVHelloText;
    ImageView mIVHelloText;

    String mHelloText;
    int mHelloImageResId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //create the view from an xml
        View rootView = inflater.inflate(R.layout.media_fragment, container, false);

        int menuId = 0;

        if(getArguments() != null) {
            menuId = getArguments().getInt(MEDIA_MENU_ID);
            mHelloText = getArguments().getString(MEDIA_TITLE);
            mHelloImageResId = getArguments().getInt(MEDIA_RES_ID);
        } else {
            menuId = mListener.getSelectedMenu();
        }

        switch (menuId) {
            case 0:
                mMediaObjects = MediasManager.getInstance().getImages();
                break;
            case 1:
                mMediaObjects = MediasManager.getInstance().getText();
                break;
            case 2:
                mMediaObjects = MediasManager.getInstance().getVideos();
                break;
            default:
                mMediaObjects = new ArrayList<MediaObject>();
                break;
        }

        mTVHelloText = (TextView) rootView.findViewById(R.id.tvHelloText);
        mIVHelloText = (ImageView) rootView.findViewById(R.id.ivHelloText);

        //set values
        if (mHelloText != null) {
            mTVHelloText.setText(mHelloText);
            mIVHelloText.setImageResource(mHelloImageResId);
        }

        //add a listener on the text
        mIVHelloText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onElementSelected();
            }
        });

        mAdapter = new MediasAdapter();
        mLVImages = (ListView)rootView.findViewById(R.id.lvImages);
        mLVImages.setAdapter(mAdapter);


        return rootView;
    }

    //set parameters for this view
    public void setHelloParameters(String text, int imageResId) {
        mHelloImageResId = imageResId;
        mHelloText = text;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;

        Activity activity;

        if (context instanceof Activity) {
            activity = (Activity) context;
            try {
                mListener = (OnElementSelectedListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
            }
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    private class MediasAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mMediaObjects.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            LinearLayout layout;

            if(view == null) {
                layout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.medias_adapter_row, null);
            } else {
                layout = (LinearLayout)view;
            }

            ImageView iv = (ImageView)layout.findViewById(R.id.ivImage);
            iv.setImageResource(R.drawable.ic_action_picture);

            TextView tvName = (TextView) layout.findViewById(R.id.tvName);
            tvName.setText("Name : " + mMediaObjects.get(i).name);

            TextView tvVersion = (TextView) layout.findViewById(R.id.tvVersion);
            tvVersion.setText("Version : " + String.valueOf(mMediaObjects.get(i).versionCode));
            Button btnDownload = (Button)layout.findViewById(R.id.btnDownloadMedia);
            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new TpAndroidInternet().downloadMediaFile(mMediaObjects.get(i), MediasFragment.this);
                }
            });

            Button btnDisplay = (Button)layout.findViewById(R.id.btnDisplayMedia);
            if(mMediaObjects.get(i).localPath == null) {
                btnDisplay.setEnabled(false);
            } else {
                btnDisplay.setEnabled(true);
                btnDisplay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), DisplayMediaActivity.class);
                        intent.putExtra("local_path", mMediaObjects.get(i).localPath);
                        getActivity().startActivity(intent);
                    }
                });
            }
            return layout;
        }
    }
}
