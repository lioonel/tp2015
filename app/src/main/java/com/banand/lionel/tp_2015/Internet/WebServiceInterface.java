package com.banand.lionel.tp_2015.Internet;

import com.banand.lionel.tp_2015.dataaccess.obj.MediaObject;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by lionelbanand on 29/01/2017.
 */

public interface WebServiceInterface {

    public static final String REMOTE_URL = "http://lionel.banand.free.fr/lp_iem/";

    @GET("/updaterLPIEM_json.php")
    List<MediaObject> getMedias(@Query("type") String type, @Query("serial") String serial);

    @GET("/search/repositories")
    List<MediaObject> searchRepos(@Query("q") String query);
}
