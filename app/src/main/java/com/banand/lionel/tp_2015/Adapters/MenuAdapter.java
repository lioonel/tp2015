package com.banand.lionel.tp_2015.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.banand.lionel.tp_2015.Items.MenuMediasItem;
import com.banand.lionel.tp_2015.R;

import java.util.ArrayList;


/**
 * Created by lionel on 30/09/14.
 */

//used to build each view for the menu
public class MenuAdapter extends BaseAdapter {

    ArrayList<MenuMediasItem> mMenuMediasItems;
    Context mContext;

    public MenuAdapter(Context context, ArrayList<MenuMediasItem> menuMediasItems) {
        mContext = context;
        mMenuMediasItems = menuMediasItems;
    }


    @Override
    public int getCount() {
        return mMenuMediasItems.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        RelativeLayout layout;

        //if it the first creation
        if (view == null) {
            layout = (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.menu_item_row, null);
        } else {
            // reuse an existing view so we need to re-set all parameters
            layout = (RelativeLayout) view;
        }

        TextView tvName = (TextView) layout.findViewById(R.id.tvName);
        ImageView ivIcon = (ImageView) layout.findViewById(R.id.ivIcon);

        //set the name and parameters regarding items of the menu
        tvName.setText(mMenuMediasItems.get(i).mName);
        ivIcon.setImageResource(mMenuMediasItems.get(i).mIcon);

        //return the layout to display
        return layout;
    }
}
