package com.banand.lionel.tp_2015.dataaccess.obj;

import com.banand.lionel.tp_2015.dataaccess.DBFLowDataBase;
import com.banand.lionel.tp_2015.Utilities.Global;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by lionel on 06/10/14.
 */
@Table(database = DBFLowDataBase.class)

public class MediaObject extends BaseModel{
    @PrimaryKey
    @Column
    public String name;
    @Column
    public int versionCode;
    @Column
    public String path;
    @Column
    public String type;
    @Column
    public String localPath;

    public String getFileExtension() {
        if(type.equals(Global.MEDIA_TYPE_IMAGE)) {
            return ".png";
        }

        return null;
    }

    public String getFileExtensionFromPath() {
        String ext = path.substring(path.length() - 4);
        return ext;
    }
}
