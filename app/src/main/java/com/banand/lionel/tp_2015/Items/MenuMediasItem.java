package com.banand.lionel.tp_2015.Items;

/**
 * Created by lionel on 30/09/14.
 */
//attribute for menu item
public class MenuMediasItem {
    public int mIcon;
    public String mName;

    public MenuMediasItem(int icon, String name) {
        mIcon = icon;
        mName = name;
    }
}
