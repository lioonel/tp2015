package com.banand.lionel.tp_2015.Service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

import com.banand.lionel.tp_2015.Internet.TpAndroidInternet;
import com.banand.lionel.tp_2015.Manager.MediasManager;
import com.banand.lionel.tp_2015.Utilities.Global;
import com.banand.lionel.tp_2015.Utilities.TpAndroidXMLParser;

import java.util.Stack;

public class TpAndroidService extends Service {

    private Stack<Bundle> mMediasList = new Stack<Bundle>();

    public TpAndroidService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                downloadMediasInfo();
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                Toast.makeText(getApplicationContext(), "Medias Infos downloaded", 2).show();
                super.onPostExecute(aVoid);
            }
        }.execute();

        return super.onStartCommand(intent, flags, startId);
    }


    private void downloadMediasInfo() {

        String xmlFile = TpAndroidInternet.downloadXMLFile(Global.MEDIA_URL);
        if (xmlFile != null && !xmlFile.equals("Not Found")) {

            TpAndroidXMLParser updaterXmlParser = new TpAndroidXMLParser();
            mMediasList = updaterXmlParser.loadUpdaterXml(xmlFile);

            MediasManager.getInstance().createMediasList(mMediasList);
        }
    }
}
